"""Base sqlalchemy schema."""

from main.factory import ma, db


class BaseSQLAlchemySchema(ma.SQLAlchemySchema):
    """
    Contain base sqlalchemy schema functionality.

    Contain schema to model conversion and field ignoring on dump.
    """

    skip_fields = {}

    class Meta:
        """Schema metadata."""

        model = None

    def make_model(self, schema_data, **kwargs) -> db.Model:
        """
        Convert data to a sqlalchemy object after successfully loading data.

        @param schema_data: The data supplied
        @param kwargs: Extra arguments
        @return: The sqlalchemy instance with the given data
        """
        return self.Meta.model(**schema_data)

    def remove_skip_values(self, schema_data, **kwargs) -> dict:
        """
        Remove fields supplied in SKIP_FIELDS.

        Those fields should not be displayed, example: a password field.
        @param schema_data: The data supplied
        @param kwargs: Extra arguments
        @return: The schema data with said fields stripped.
        """
        return {
            key: field_value for key, field_value in schema_data.items() if key not in self.skip_fields
        }
