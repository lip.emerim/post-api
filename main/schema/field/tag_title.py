"""Tag field filter. Trim and lowercase the string to avoid discrepancies."""

from marshmallow import fields


# noinspection PyCallingNonCallable
class TagTitle(fields.String):
    """Tag field filter. Trim and lowercase the string to avoid discrepancies."""

    def _deserialize(self, field_value, *args, **kwargs):
        if hasattr(field_value, 'strip'):
            field_value = field_value.strip()

        if hasattr(field_value, 'lower'):
            field_value = field_value.lower()

        return super()._deserialize(field_value, *args, **kwargs)
