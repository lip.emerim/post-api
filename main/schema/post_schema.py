"""Represent a post."""
from marshmallow import post_load, post_dump
from marshmallow.validate import Length

from main.schema.base_schema import BaseSQLAlchemySchema
from main.factory import ma
from main.model.post_model import PostModel
from main.schema.tag_schema import TagSchema
from main.schema.user_schema import UserSchema


class PostSchema(BaseSQLAlchemySchema):
    """Represent a post."""

    class Meta:
        """Schema metadata."""

        model = PostModel

    id = ma.auto_field(dump_only=True)
    title = ma.auto_field(required=True, validate=Length(min=3, max=50))
    body = ma.auto_field(required=True, validate=Length(min=5, max=1000))
    tags = ma.Nested(TagSchema, many=True, required=False, validate=Length(max=100))
    user = ma.Nested(UserSchema, required=False)

    @post_load
    def make_model(self, schema_data, **kwargs) -> PostModel:

        schema_data['tags'] = list(set(schema_data.get('tags', [])))
        return super().make_model(schema_data, **kwargs)

    @post_dump
    def remove_skip_values(self, schema_data, **kwargs) -> dict:
        return super().remove_skip_values(schema_data, **kwargs)


post_schema = PostSchema()
posts_schema = PostSchema(many=True)
