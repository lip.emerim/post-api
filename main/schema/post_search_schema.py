"""Validate post search attributes."""

from marshmallow import fields
from marshmallow.validate import Range, Length

from main.factory import ma
from main.schema.field.tag_title import TagTitle


class PostSearchSchema(ma.Schema):
    """Validate post search attributes."""

    tag = TagTitle(required=False, validate=Length(min=3, max=20))
    start = fields.Integer(required=False, validate=Range(min=0), missing=0)
    length = fields.Integer(required=False, validate=Range(min=0, max=20), missing=10)


post_search_schema = PostSearchSchema()
