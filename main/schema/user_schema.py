"""Represent a user."""

from marshmallow import validate, ValidationError, post_load, post_dump, validates_schema
from marshmallow.validate import Length, Regexp

from main.schema.base_schema import BaseSQLAlchemySchema
from main.factory import ma
from main.model.user_model import UserModel


class UserSchema(BaseSQLAlchemySchema):
    """Represent a user."""

    skip_fields = {'password'}

    class Meta:
        """Schema metadata."""

        model = UserModel

    id = ma.auto_field(dump_only=True)
    email = ma.auto_field(required=True, validate=[validate.Email()])
    password = ma.auto_field(
        required=True,
        validate=[
            Length(min=8),
            Regexp(
                regex=r'^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$',
                error='Password must contain at least a letter, a number and a special character',
            ),
        ])

    @validates_schema
    def validate_unique_email(self, schema_data, **kwargs):
        """
        Validate that an email is unique in our database.

        If the user is new (id is None) we check if the email exists.
        """
        email = schema_data.get('email')

        query = UserModel.query.filter(UserModel.email == email)

        exists = query.first()

        if exists:
            raise ValidationError('Email already taken')

    @post_load
    def make_model(self, schema_data, **kwargs) -> UserModel:
        return super().make_model(schema_data, **kwargs)

    @post_dump
    def remove_skip_values(self, schema_data, **kwargs) -> dict:
        return super().remove_skip_values(schema_data, **kwargs)


user_schema = UserSchema()
users_schema = UserSchema(many=True)
