"""Represent a tag."""
from marshmallow import post_load, post_dump
from marshmallow.validate import Length

from main.schema.base_schema import BaseSQLAlchemySchema
from main.factory import ma
from main.model.tag_model import TagModel
from main.schema.field.tag_title import TagTitle


class TagSchema(BaseSQLAlchemySchema):
    """Represent a tag."""

    class Meta:
        """Schema metadata."""

        model = TagModel

    id = ma.auto_field(dump_only=True)
    title = TagTitle(required=True, validate=Length(min=3, max=20))

    @post_load
    def make_model(self, schema_data, **kwargs) -> TagModel:
        return super().make_model(schema_data, **kwargs)

    @post_dump
    def remove_skip_values(self, schema_data, **kwargs) -> dict:
        return super().remove_skip_values(schema_data, **kwargs)


tag_schema = TagSchema()
tags_schema = TagSchema(many=True)
