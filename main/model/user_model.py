"""Represent the user database table."""

from main.factory import db
from main.model.post_model import PostModel


class UserModel(db.Model):
    """Represent the user database table."""

    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.Text, unique=True, index=True)
    password = db.Column('password', db.Text)
    posts = db.relationship('PostModel', backref=db.backref('user'), lazy='select', cascade='all, delete-orphan')
