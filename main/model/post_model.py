"""Represent the post database table."""

from main.factory import db
from main.model.tag_model import TagModel

post_tags = db.Table(
    'post_tags',
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id', ondelete='CASCADE'), primary_key=True),
    db.Column('post_id', db.Integer, db.ForeignKey('post.id', ondelete='CASCADE'), primary_key=True),
)


class PostModel(db.Model):
    """Represent the post database table."""

    __tablename__ = 'post'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, unique=False)
    body = db.Column(db.Text, unique=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'), index=True)
    tags = db.relationship(
        'TagModel',
        secondary=post_tags,
        lazy='subquery',
        passive_deletes=True,
        backref=db.backref('posts', lazy='select', cascade_backrefs=False),
    )
