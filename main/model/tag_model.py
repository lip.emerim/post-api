"""Represent the tag database table."""

from main.factory import db


class TagModel(db.Model):
    """Represent the tag database table."""

    __tablename__ = 'tag'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, unique=True, index=True)

    def key(self):
        """Generate hash key."""
        return tuple(self.title)

    def __hash__(self):
        """Generate object hash."""
        return hash(self.key())

    def __eq__(self, other: 'TagModel'):
        """
        Override object equals.

        This is used to create a set of unique tags on insertion of data.
        """
        if isinstance(other, TagModel):
            return self.key() == other.key()
        return NotImplemented
