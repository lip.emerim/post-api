"""Define user related routes."""
from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from main.schema.user_schema import user_schema
from main.service import user_service

user_blueprint = Blueprint('user_blueprint', __name__, url_prefix='/user')


@user_blueprint.route('', methods=['POST'])
def save_user():
    """
    Create a user.

    Perform validation and status mapping of the incoming data.
    See UserSchema

    @return: user object in case of success, or an object containing the data violations
    """
    try:
        user = user_schema.load(request.json)
    except ValidationError as err:
        return jsonify(err.messages), 422

    user = user_service.save(user)

    return jsonify(user_schema.dump(user)), 201
