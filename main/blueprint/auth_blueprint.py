"""Define authentication related routes."""
from flask import Blueprint, request, jsonify

from main.service import auth_service

auth_blueprint = Blueprint('auth_blueprint', __name__, url_prefix='/auth')


@auth_blueprint.route('/login', methods=['POST'])
def login():
    """
    Login a user.

    The email and password parameters must be supplied via JSON.

    Returns an authentication token in case the credentials are valid
    """
    request_data = request.get_json()
    email = request_data.get('email', None)
    password = request_data.get('password', None)

    token = auth_service.authenticate(email, password)

    if not token:
        return jsonify({'message': 'Invalid credentials'})

    return jsonify({'access_token': token.decode()})
