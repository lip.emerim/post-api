"""Define post related routes."""
from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from main.factory import token_auth
from main.schema.post_schema import post_schema, posts_schema
from main.schema.post_search_schema import post_search_schema
from main.service import post_service

post_blueprint = Blueprint('post_blueprint', __name__, url_prefix='/post')


@post_blueprint.route('/<int:post_id>', methods=['GET'])
def get_post(post_id):
    """Retrieve a post by id."""
    post = post_service.find_by_id(post_id)
    return jsonify(post_schema.dump(post))


@post_blueprint.route('', methods=['GET'])
def search():
    """
    Search for posts.

    May have 3 parameters:
      - start: Indicates the offset of the result, default 0
      - length: Indicates the length of the result, default 20
      - tag: A tag title to search for, if not supplied, no filter is made
    """
    try:
        request_data = post_search_schema.load(request.args)
    except ValidationError as err:
        return jsonify(err.messages), 422

    tag = request_data.get('tag', None)
    start = request_data.get('start')
    length = request_data.get('length')

    search_data = post_service.search(start, length, tag)

    search_data['items'] = posts_schema.dump(search_data['items'])

    return jsonify(search_data)


@post_blueprint.route('', methods=['POST'])
@token_auth.login_required
def save_post():
    """
    Create a post.

    Save a post with the given data, the post user is set dynamically
    according to the current logged in user.
    See PostSchema

    @return: post object in case of success, or an object containing the data violations
    """
    try:
        post = post_schema.load(request.json)
    except ValidationError as err:
        return jsonify(err.messages), 422

    post = post_service.save(post, int(token_auth.current_user().id))

    return jsonify(post_schema.dump(post)), 201


@post_blueprint.route('/<int:post_id>', methods=['DELETE'])
@token_auth.login_required
def delete_post(post_id):
    """
    Delete a post.

    Only the owner of a post can delete it.
    """
    post_service.delete(post_id, token_auth.current_user().id)

    return jsonify(''), 204
