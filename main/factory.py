"""The heart of the application, defines the create_app and initialize all extensions."""

import logging
import traceback
from logging.handlers import RotatingFileHandler

from environs import Env
from flask import Flask, jsonify
from flask_httpauth import HTTPTokenAuth
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from werkzeug.exceptions import HTTPException, default_exceptions

migrate = Migrate()
db = SQLAlchemy()
ma = Marshmallow()
env = Env()
env.read_env()
token_auth = HTTPTokenAuth(scheme='Bearer')

MAX_LOG_FILE_BYTES = 100000
LOG_BACKUPS = 5


def create_app():
    """
    Create an instance of the flask app.

    Create the flask app, configure the logger, bind extensions to app object,
    push app context to celery tasks and register all blueprints.

    :return: An instance of Flask
    """
    app = Flask(__name__)
    app.config.from_object('config.Config')

    # Sqlalchemy models need to be imported before binding it with the current app
    # Otherwise sqlalchemy create and drop all wont work, migrations wont work

    # TODO import models here
    from main.model.tag_model import TagModel
    from main.model.post_model import PostModel
    from main.model.user_model import UserModel

    # TODO import blueprints here
    from main.blueprint.user_blueprint import user_blueprint
    from main.blueprint.post_blueprint import post_blueprint
    from main.blueprint.auth_blueprint import auth_blueprint

    app.register_blueprint(user_blueprint, url_prefix='/user')
    app.register_blueprint(post_blueprint, url_prefix='/post')
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    setup_logging(app)
    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    setup_error_handlers(app)

    return app


def setup_logging(app):
    """Set logger when not in debug mode."""
    if not app.debug:
        log_handler = RotatingFileHandler('post-api.log', maxBytes=MAX_LOG_FILE_BYTES, backupCount=LOG_BACKUPS)
        log_handler.setLevel(logging.INFO)
        app.logger.addHandler(log_handler)


def setup_error_handlers(app):
    """Set app HTTP error handlers."""
    @app.errorhandler(Exception)
    def handle_error(error):
        """Handle every application error."""
        app.logger.error(traceback.format_exc())
        code = 500
        message = 'internal server error'
        if isinstance(error, HTTPException):
            code = error.code
            message = error
        return jsonify({'success': False, 'message': str(message)}), code

    for ex in default_exceptions:
        app.register_error_handler(ex, handle_error)
