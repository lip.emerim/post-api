"""Contain business logic related to auth."""
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired, BadSignature
from werkzeug.exceptions import abort
from werkzeug.security import check_password_hash

from config import Config
from main.factory import token_auth
from main.model.user_model import UserModel


@token_auth.verify_token
def verify_token(token):
    """
    Check that the token supplied is valid.

    @param token: The token to check for.

    @return: User info if the token is valid or None in case of an invalid token.
    """
    user = verify_auth_token(token)

    if not user:
        return None

    return user


@token_auth.error_handler
def auth_error(status):
    """Abort with a 403 error when authentication fails."""
    return abort(status)


def authenticate(email: str, password: str):
    """
    Authenticate a user.

    Generate an access token in case of valid credentials

    @param email: The user email
    @param password: The user password
    @return: The user access token
    """
    user = UserModel.query.filter(UserModel.email == email).first()

    if user is None:
        return None

    if not check_password_hash(user.password, password):
        return None

    return generate_auth_token(user)


def generate_auth_token(user, expiration=600):
    """
    Generate an auth token for a given user.

    The user id is embedded into the token so that when we
    retrieve it, we can find the user it belongs to

    @param user: The user that the token will be generated to
    @param expiration: The expiration time of the token
    """
    serializer = Serializer(Config.SECRET_KEY, expires_in=expiration)
    return serializer.dumps({'id': user.id})


def verify_auth_token(token):
    """
    Check if a token is valid and retrieve a user for a given token.

    @param token: The token to check for
    @return: The user object if the token is valid, otherwise None
    """
    serializer = Serializer(Config.SECRET_KEY)
    try:
        user_data = serializer.loads(token)
    except SignatureExpired:
        return None  # valid token, but expired
    except BadSignature:
        return None  # invalid token
    return UserModel.query.get(user_data['id'])
