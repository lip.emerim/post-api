"""Contain business logic related to user."""
from werkzeug.security import generate_password_hash

from main.factory import db
from main.model.user_model import UserModel


def save(user: UserModel) -> UserModel:
    """
    Add a new user.

    @param user: The user object with the insert info
    @return: None
    """
    user.password = generate_password_hash(user.password)
    db.session.add(user)
    db.session.commit()

    return user
