"""Contain business logic related to post."""
from typing import Optional

from sqlalchemy import not_
from werkzeug.exceptions import abort

from main.factory import db
from main.model.post_model import PostModel
from main.model.tag_model import TagModel

MAX_PAGE_LENGTH = 20


def save(post: PostModel, user_id: int):
    """
    Add a new post.

    @param post: The post object with the insert info
    @param user_id: The id of the logged in user
    @return: None
    """
    post.user_id = user_id
    post.tags = get_tags(post.tags)
    db.session.add(post)
    db.session.commit()

    return post


def delete(post_id: int, user_id: int):
    """Remove a post."""
    post = PostModel.query.filter(PostModel.id == post_id).first_or_404()

    if post.user_id != user_id:
        abort(403)  # noqa: WPS432, http magic number is ok

    db.session.delete(post)
    remove_orphan_tags()
    db.session.commit()


def remove_orphan_tags():
    """Remove tags that do not have any post anymore."""
    db.session.query(TagModel).filter(
        not_(TagModel.posts.any()),
    ).delete(synchronize_session=False)


def get_tags(tags: list):
    """
    Get tags of a post before insertion.

    Get existing tags from the database and merge with post tags.

    @param tags: The post tags
    @return: A list containing both db and new tags
    """
    titles = [tag.title for tag in tags]
    db_tags = TagModel.query.filter(TagModel.title.in_(titles)).all()

    new_tags = [tag for tag in tags if tag not in db_tags]

    return db_tags + new_tags


def find_by_id(post_id: int) -> PostModel:
    """
    Find a post by a given id or raises 404 error.

    @param post_id: The id to search for.
    """
    return PostModel.query.filter(PostModel.id == post_id).first_or_404()


def search(start: int = 0, length: int = MAX_PAGE_LENGTH, tag: Optional[str] = None):
    """
    Search for posts.

    @param start: The index to start the search, defaults to 0
    @param length: The length of the search, max=20, default=20
    @param tag: An optional tag to filter the results

    @return: A dict containing the items and search information:

    {
        items: [...],
        total_results: int,
        start: int,
        length: int,

    }

    """
    query = PostModel.query

    if tag is not None:
        query = query.filter(PostModel.tags.any(title=tag))

    query = query.order_by(PostModel.id.desc())

    total_results = query.count()

    query = query.limit(length).offset(start)
    result_set = query.all()

    return {
        'items': result_set,
        'total_results': total_results,
        'start': start,
        'length': length,
    }
