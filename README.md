# Post API

[![pipeline status](https://gitlab.com/lip.emerim/post-api/badges/master/pipeline.svg)](https://gitlab.com/lip.emerim/post-api/-/commits/master)
[![coverage report](https://gitlab.com/lip.emerim/post-api/badges/master/coverage.svg)](https://gitlab.com/lip.emerim/post-api/-/commits/master)

## Running dev app

Copy the contents of .env.example to .env

    cp .env.example .env
    
Run docker-compose up

    docker-compose up
    
This will setup flask development server, a database
with persistent data and real time code changes.

After you run this command you should run the migrations

    docker-compose run --rm app flask db upgrade
    
## Running production-like app

    docker-compose -f docker-compose.yaml up
    
This initializes the app in an environment more similar
to production. It setups Uwsgi to run behind an Nginx,
there are no mounted volumes and it assumes an external
database. To see code changes it is necessary to rebuild
the containers. If you want to use the container database
it needs to be previously initialized. In this mode, 
database migrations always run on startup.

## Start the database

Some steps (like running tests and migrations) 
will required the database service to be
up. You can start it beforehand on another terminal with:

    docker-compose up database

## Running tests

    docker-compose run --rm app nose2
    
## Running tests with coverage report
    docker-compose run --rm app nose2 --with-coverage
    
That will generate the docs on coverage_html_report. You can open them using
you preferred browser. For Example

    firefox coverage_html_report/index.html

## Running wemake linter

    docker-compose run --rm app flake8

## Generate python docs

Enter the app docker container
    
    docker-compose run --rm app bash
    
Then run
    
    cd docs
    sphinx-apidoc -f -o . ../
    make html
The docs will be available on the file docs/_build/html/index.html.
You can open it using your preferred browser

    firefox docs/_build/html/index.html

## Dev commands

### Create database

    docker-compose run --rm app flask create_db

### Drop database

    docker-compose run --rm app flask drop_db

## Migrations

The migration engine we are using is flask-migrate. Please
see the available docs

## Endpoints

Here are the endpoints of the application, in the future this should be in a
openapi specification.

### User

#### POST /user

Create a new user, example:

request

```json
{
  "email": "john.doe@gmail",
  "password": "12345678a&"
}
```

response 201
```json
{
  "id": 1,
  "email": "john.doe@gmail"
}
```

### Authentication

#### POST /auth/login

Login an user on the system

request:

```json
{
  "email": "john.doe@gmail",
  "password": "12345678a&"
}
```

response: 200
```json
{
  "access_token": "..."
}
```

### Post

#### POST /post

Create a post
```json
{
  "title": "My amazing post",
  "body": "This is a really amazing post, and I really mean it",
  "tags": [
    {
      "title": "amazing"
    } 
  ]
}
```


response: 201

```json
{
  "id": 1,
  "title": "My amazing post",
  "body": "This is a really amazing post, and I really mean it",
  "tags": [
    {
      "id": 1,
      "title": "amazing"
    } 
  ]
}
```


### GET /post/{id}

Get a post by id

response 200
```json
{
  "id": 1,
  "title": "My amazing post",
  "body": "This is a really amazing post, and I really mean it",
  "tags": [
    {
      "id": 1,
      "title": "amazing"
    } 
  ]
}
```

### DELETE /post/{id}

response 204 empty body

### GET /post

List posts

request: (all parameters are optional)

```json
{
  "tag": "amazing",
  "start": 0,
  "length": 10
}
```

response:

```json
{
  "items": [
    {
      "id": 1,
      "title": "My amazing post",
      "body": "This is a really amazing post, and I really mean it",
      "tags": [
        {
          "id": 1,
          "title": "amazing"
        }
      ]
    }
  ],
  "total_results": 50,
  "start": 0,
  "length": 10
}
```

### Field error

response 422



```json
{
  "email": [
    "Not a valid email address."
  ],
  "password": [
    "Shorter than minimum length 8.",
    "Password must contain at least a letter, a number and a special character"
  ]
}
```

### HTTP errors

```json
  {
    "success": false,
    "message": "HTTP error message"
  }
  
```