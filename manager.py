"""Contain post app initialization logic."""

import cli
from main.factory import create_app, db

flask_app = create_app()
cli.register(flask_app, db)
