"""Configures the application and all of its modules, database connection and env var configs."""

import os

from main.factory import env


class Config:
    """Define the application config parameters."""

    POSTGRES_URL = env.str('POSTGRES_URL')
    POSTGRES_USER = env.str('POSTGRES_USER')
    POSTGRES_PW = env.str('POSTGRES_PW')
    POSTGRES_DB = env.str('POSTGRES_DB')
    POSTGRES_PORT = env.str('POSTGRES_PORT')
    TESTING = env.bool('TESTING')
    DEBUG = env.bool('DEBUG')

    # To turn on query logging set the config SQLALCHEMY_ECHO = True

    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://{user}:{pw}@{url}:{port}/{db}'.format(
        user=POSTGRES_USER,
        pw=POSTGRES_PW,
        url=POSTGRES_URL,
        db=POSTGRES_DB,
        port=POSTGRES_PORT,
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # silence the deprecation warning
    SQLALCHEMY_ENGINE_OPTIONS = {
        'pool_pre_ping': env.bool('SQLALCHEMY_POOL_PRE_PING'),
        'pool_size': env.int('SQLALCHEMY_POOL_SIZE'),
        'pool_recycle': env.int('SQLALCHEMY_POOL_RECYCLE'),
        'max_overflow': env.int('SQLALCHEMY_MAX_OVERFLOW'),
        'echo_pool': env.bool('SQLALCHEMY_ECHO_POOL'),
    }

    SECRET_KEY = os.urandom(16)

    # flask debug toolbar configurations
    DEBUG_TB_PROFILER_ENABLED = True
    DEBUG_TB_TEMPLATE_EDITOR_ENABLED = True
