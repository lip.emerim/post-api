"""Contain flask commands that are called from shell."""


def register(app, db):
    """Register the application shell commands."""
    @app.cli.command('create_db')
    def init_db():  # noqa:W0612 pylint thinks it is an unused attribute
        """Create all sqlalchemy tables imported in the factory.py of the main module."""
        db.metadata.create_all(db.engine, checkfirst=True)

    @app.cli.command('drop_db')
    def drop_db():  # noqa:W0612 pylint thinks it is an unused attribute
        """Drop all sqlalchemy tables imported in the factory.py of the main module."""
        db.metadata.drop_all(db.engine, checkfirst=True)
