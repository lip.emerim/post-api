"""Define auth service tests."""

from assertpy import assert_that
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired
from werkzeug.exceptions import Forbidden

from main.model.user_model import UserModel
from main.service import auth_service
from test.base_test import BaseTest


class AuthServiceTest(BaseTest):
    """Define auth service tests."""

    def setUp(self) -> None:
        """Set the instance serializer."""
        super().setUp()
        self.serializer = Serializer(self.app.config['SECRET_KEY'], expires_in=600)

    def test_verify_auth_token_valid_token(self):
        """Check a valid token is correctly verified."""
        token = self.serializer.dumps({'id': 1})

        db_user = UserModel.query.filter(UserModel.id == 1).first()

        user = auth_service.verify_auth_token(token)

        assert_that(user).is_not_none()
        assert_that(user.id).is_equal_to(1)
        assert_that(user.email).is_equal_to(db_user.email)

    def test_verify_auth_token_expired(self):
        """Check an expired token fails verification."""
        self.serializer.expires_in = -1

        token = self.serializer.dumps({'id': 1})

        user = auth_service.verify_auth_token(token)

        assert_that(user).is_none()

        self.serializer.expires_in = 600

    def test_verify_auth_token_invalid(self):
        """Check an invalid token fails verification."""
        token = 'not_realy_valid'

        user = auth_service.verify_auth_token(token)

        assert_that(user).is_none()

    def test_generate_auth_token(self):
        """Test the auth token is correctly generated."""
        user = UserModel.query.first()

        token = auth_service.generate_auth_token(user)

        user_id = self.serializer.loads(token)['id']

        assert_that(user_id).is_equal_to(user.id)

    def test_generate_auth_token_expiration(self):
        """
        Test the token expiration is working.

        To do this, we set a negative expiration to the generate_auth_token method.
        That way the token should always be expired
        """
        user = UserModel.query.first()

        token = auth_service.generate_auth_token(user, expiration=-1)

        assert_that(
            self.serializer.loads,
        ).raises(SignatureExpired).when_called_with(token)

    def test_verify_token_invalid(self):
        """Test access is not granted on invalid token."""
        token = 'asdfsadfdsfds'

        user = auth_service.verify_token(token)

        assert_that(user).is_none()

    def test_verify_auth_token(self):
        """Test access is granted on valid token."""
        token = self.serializer.dumps({'id': 1})

        user = auth_service.verify_token(token)

        assert_that(user).is_not_none()
        assert_that(user.id).is_equal_to(1)

    def test_auth_error(self):
        """Test the auth error handler is working properly."""
        assert_that(auth_service.auth_error).raises(
            Forbidden,
        ).when_called_with(403)

    def test_authenticate(self):
        """Test the authentication is working properly."""
        token = auth_service.authenticate('john@gmail.com', '123456')

        assert_that(token).is_not_none()

        user_id = self.serializer.loads(token)['id']

        user = UserModel.query.filter(UserModel.id == user_id).first()

        assert_that(user).is_not_none()

        assert_that(user.email).is_equal_to('john@gmail.com')

    def test_authenticate_invalid_email(self):
        """Test an invalid email will not authenticate."""
        token = auth_service.authenticate('not right', '123456')

        assert_that(token).is_none()

    def test_authenticate_invalid_password(self):
        """Test an invalid password will not authenticate."""
        token = auth_service.authenticate('john@gmail.com', 'not right')

        assert_that(token).is_none()
