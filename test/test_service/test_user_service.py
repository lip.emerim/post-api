"""Define user service tests."""

from assertpy import assert_that

from main.model.user_model import UserModel
from main.service import user_service
from test.base_test import BaseTest


class UserServiceTest(BaseTest):
    """Define user service tests."""

    def test_save_user(self):
        """Test user saving is working correctly."""
        user = UserModel(email='new@gmail.com', password='1234')

        user_service.save(user)

        user = UserModel.query.filter(UserModel.email == 'new@gmail.com').first()

        assert_that(user).is_not_none()
        assert_that(user.password).is_not_equal_to('1234')
