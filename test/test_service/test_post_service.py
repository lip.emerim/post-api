"""Define post service tests."""

from assertpy import assert_that
from sqlalchemy import inspect
from werkzeug.exceptions import NotFound, Forbidden

from main.model.post_model import PostModel
from main.model.tag_model import TagModel
from main.service import post_service
from test.base_test import BaseTest


class PostServiceTest(BaseTest):
    """Define post service tests."""

    def test_add_post(self):
        """Test the add post method is working."""
        tags = TagModel.query.limit(2).all()
        body = self.faker.text()

        post = PostModel(
            title='Amazing post',
            body=body,
            tags=tags,
        )

        post_service.save(post, 1)

        post = PostModel.query.filter(PostModel.title == 'Amazing post').first()

        assert_that(post).is_not_none()
        assert_that(post.user_id).is_equal_to(1)
        assert_that(post.body).is_equal_to(body)
        assert_that(post.tags).is_equal_to(tags)

    def test_delete_post(self):
        """Test the delete post method is working."""
        post = PostModel.query.filter(PostModel.id == 1).first()

        assert_that(post).is_not_none()

        post_service.delete(1, post.user_id)

        db_post = PostModel.query.filter(PostModel.id == 1).first()

        assert_that(db_post).is_none()

    def test_delete_post_not_exists(self):
        """Test a non existing post deletion raises NotFound error."""
        assert_that(post_service.delete).raises(NotFound).when_called_with(234, 1)

    def test_delete_post_not_owner(self):
        """Test a non owner post deletion raises Forbidden error."""
        assert_that(post_service.delete).raises(Forbidden).when_called_with(1, 234)

    def test_remove_orphan_tags(self):
        """Test orphan tags are being correctly removed."""
        PostModel.query.filter(PostModel.tags.any(title='news')).delete(synchronize_session=False)
        self.db.session.commit()

        post_service.remove_orphan_tags()
        self.db.session.commit()

        assert_that(TagModel.query.all()).is_length(4)

        tag = TagModel.query.filter(TagModel.title == 'news').all()

        assert_that(tag).is_empty()

    def test_get_tags(self):
        """
        Test the get tags function is correctly working.

        To do this we supply some new tags and some existing tags.
        The result should be a merge of persistent and transient tags.
        @return: None
        """
        tag1 = TagModel(title='does_not_exist')
        tag2 = TagModel(title='news')

        tags = post_service.get_tags([tag1, tag2])

        for tag in tags:
            if tag.title == 'news':
                assert_that(inspect(tag).persistent).is_true()

            if tag.title == 'does_not_exist':
                assert_that(inspect(tag).persistent).is_false()
                assert_that(inspect(tag).transient).is_true()

    def test_search_no_parameters(self):
        """Test the search works with no parameters."""
        search_data = post_service.search()

        assert_that(search_data['items']).is_length(5)
        assert_that(search_data['start']).is_equal_to(0)
        assert_that(search_data['length']).is_equal_to(post_service.MAX_PAGE_LENGTH)
        assert_that(search_data['total_results']).is_equal_to(5)

    def test_search_custom_start(self):
        """Test the search works with a start parameter."""
        search_data = post_service.search()
        search_data_start = post_service.search(start=2)

        assert_that(search_data_start['items']).is_length(3)
        assert_that(search_data_start['start']).is_equal_to(2)
        assert_that(search_data_start['total_results']).is_equal_to(5)

        search_ids = [post.id for post in search_data_start['items']]

        assert_that(search_ids).does_not_contain(search_data['items'][0].id)
        assert_that(search_ids).does_not_contain(search_data['items'][1].id)

        assert_that(search_ids).contains(search_data['items'][2].id)
        assert_that(search_ids).contains(search_data['items'][3].id)
        assert_that(search_ids).contains(search_data['items'][4].id)

    def test_search_data_custom_length(self):
        """Test the search works with a length parameter."""
        search_data = post_service.search(length=2)

        assert_that(search_data['items']).is_length(2)
        assert_that(search_data['total_results']).is_equal_to(5)

    def test_search_data_custom_tag(self):
        """Test the search works with a tag parameter."""
        search_data = post_service.search(tag='news')

        assert_that(search_data['items']).is_length(2)
        assert_that(search_data['total_results']).is_equal_to(2)

        tag_count = PostModel.query.filter(PostModel.tags.any(title='news')).count()

        assert_that(tag_count).is_equal_to(search_data['total_results'])

        tag = TagModel(title='news')
        for post in search_data['items']:
            assert_that(post.tags).contains(tag)

    def test_find_by_id(self):
        """Test the method find by id is working properly."""
        post = PostModel.query.filter(PostModel.id == 1).first()

        service_post = post_service.find_by_id(post.id)

        assert_that(post.id).is_equal_to(service_post.id)
        assert_that(post.title).is_equal_to(service_post.title)
        assert_that(post.tags).is_equal_to(service_post.tags)
        assert_that(post.body).is_equal_to(service_post.body)
        assert_that(post.user_id).is_equal_to(service_post.user_id)

    def test_find_by_id_not_exists(self):
        """Test a NotFound exception is raised when a non existing post id is supplied."""
        assert_that(post_service.find_by_id).raises(NotFound).when_called_with(1231)
