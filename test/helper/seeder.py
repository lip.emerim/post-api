"""Define database seeding."""

from werkzeug.security import generate_password_hash

from main.model.post_model import PostModel
from main.model.tag_model import TagModel
from main.model.user_model import UserModel


def seed(db, faker):
    """Populate the database with fake, but controlled data."""
    password = generate_password_hash('123456')

    user1 = UserModel(email='john@gmail.com', password=password)
    user2 = UserModel(email='doe@gmail.com', password=password)

    post1 = PostModel(
        title=faker.text(max_nb_chars=10),
        body=faker.text(),
    )

    post2 = PostModel(
        title=faker.text(max_nb_chars=10),
        body=faker.text(),
    )

    post3 = PostModel(
        title=faker.text(max_nb_chars=10),
        body=faker.text(),
    )

    post4 = PostModel(
        title=faker.text(max_nb_chars=10),
        body=faker.text(),
    )

    post5 = PostModel(
        title=faker.text(max_nb_chars=10),
        body=faker.text(),
    )

    post1.user = user1
    post3.user = user1
    post5.user = user1
    post2.user = user2
    post4.user = user2

    tag1 = TagModel(title='news')
    tag2 = TagModel(title='controversy')
    tag3 = TagModel(title='games')
    tag4 = TagModel(title='movies')
    tag5 = TagModel(title='rants')

    post1.tags = [tag1, tag2]
    post2.tags = [tag3, tag4]
    post3.tags = [tag5, tag1]
    post4.tags = [tag2, tag3]
    post5.tags = [tag4, tag5]

    db.session.add_all([
        post1,
        post2,
        post3,
        post4,
        post5,
        tag1,
        tag2,
        tag3,
        tag4,
        tag5,
        user1,
        user2,
    ])

    db.session.commit()
