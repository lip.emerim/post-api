"""Define default testing behaviors."""

import unittest

from faker import Faker

from main.factory import create_app, db
from test.helper import seeder


class BaseTest(unittest.TestCase):
    """Contain common behaviors for every test."""

    def setUp(self) -> None:
        """
        Define setup for tests.

        Create app, database, folder and push app context to tests.

        :return: None
        """
        self.app = create_app()
        self.app_context = self.app.app_context()
        self.client = self.app.test_client()
        self.app_context.push()
        self.faker = Faker()
        self.db = db
        self.db.metadata.drop_all(db.engine, checkfirst=True)
        self.db.create_all()
        seeder.seed(self.db, self.faker)

    def tearDown(self) -> None:
        """
        Define TearDown for tests.

        Drop the database, remove folders and app context.

        :return: None
        """
        self.db.session.remove()
        self.db.metadata.drop_all(db.engine, checkfirst=True)
        self.app_context.pop()
