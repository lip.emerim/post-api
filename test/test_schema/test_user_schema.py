"""Define user schema tests."""

import simplejson
from assertpy import assert_that
from marshmallow import ValidationError

from main.model.user_model import UserModel
from main.schema.user_schema import user_schema
from test.base_test import BaseTest


class UserSchemaTest(BaseTest):
    """Define user schema tests."""

    def test_load_user(self):
        """Test a user is being correctly loaded."""
        user_data = {
            'email': 'romero@gmail.com',
            'password': 'Acdfe124@',
        }

        user_data = simplejson.dumps(user_data)

        user = user_schema.loads(user_data)

        assert_that(user.email).is_equal_to('romero@gmail.com')

    def test_load_user_invalid_email(self):
        """Test a ValidationError is raise when loading a user with invalid email."""
        user_data = {
            'email': 'nonono',
            'password': 'Acdfe124@',
        }

        user_data = simplejson.dumps(user_data)

        assert_that(user_schema.loads).raises(
            ValidationError,
        ).when_called_with(user_data)

    def test_load_user_invalid_password(self):
        """Test a ValidationError is raise when loading a user with invalid password."""
        user_data = {
            'email': 'romero@gmail.com',
            'password': 'nonono',
        }

        user_data = simplejson.dumps(user_data)

        assert_that(user_schema.loads).raises(
            ValidationError,
        ).when_called_with(user_data)

    def test_load_user_existing_email_no_id(self):
        """Test a ValidationError is raise when loading a user with an existing email."""
        user_data = {
            'email': 'john@gmail.com',
            'password': 'Acdfe124@',
        }

        user_data = simplejson.dumps(user_data)

        assert_that(user_schema.loads).raises(
            ValidationError,
        ).when_called_with(user_data)

    def test_password_not_dumped(self):
        """Test the user password is not dumped."""
        user = UserModel.query.first()

        schema_user = user_schema.dump(user)

        assert_that(schema_user).does_not_contain_key('password')
