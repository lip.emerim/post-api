"""Define post schema tests."""

import simplejson
from assertpy import assert_that
from marshmallow import ValidationError

from main.model.tag_model import TagModel
from main.schema.post_schema import post_schema
from test.base_test import BaseTest


class PostSchemaTest(BaseTest):
    """Define post service tests."""

    def test_load_post_invalid_title(self):
        """Test a ValidationError is raise when loading a post with invalid title."""
        post_data = {
            'title': 'na',
            'body': self.faker.text(),
        }

        post_data = simplejson.dumps(post_data)

        assert_that(post_schema.loads).raises(
            ValidationError,
        ).when_called_with(post_data)

    def test_load_post_invalid_body(self):
        """Test a ValidationError is raise when loading a post with invalid body."""
        post_data = {
            'title': self.faker.text(10),
            'body': 'lou',
        }

        post_data = simplejson.dumps(post_data)

        assert_that(post_schema.loads).raises(
            ValidationError,
        ).when_called_with(post_data)

    def test_load_post_duplicate_tags(self):
        """Test that duplicate tags are merged when loading the post."""
        post_data = {
            'title': self.faker.text(10),
            'body': self.faker.text(),
            'tags': [
                {
                    'title': 'news',
                },
                {
                    'title': '  news',
                },
                {
                    'title': 'news   ',
                },
                {
                    'title': 'NeWs',
                },
                {
                    'title': '   NeWs  ',
                },
                {
                    'title': 'controversy',
                },
            ],
        }

        post_data = simplejson.dumps(post_data)

        post = post_schema.loads(post_data)

        assert_that(post.tags).is_length(2)

        assert_that(post.tags).contains(TagModel(title='news'))
        assert_that(post.tags).contains(TagModel(title='controversy'))
