"""Define auth blueprint tests."""

from unittest import mock

import simplejson
from assertpy import assert_that
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from test.base_test import BaseTest


class AuthBlueprintTest(BaseTest):
    """Define auth blueprint tests."""

    def setUp(self) -> None:
        """Add token serializer to instance."""
        super().setUp()
        self.serializer = Serializer(self.app.config['SECRET_KEY'], expires_in=600)

    @mock.patch('main.service.auth_service.authenticate')
    def test_login_valid(self, authenticate):
        """Test a valid login is working properly."""
        token = self.serializer.dumps({'id': 1})

        authenticate.return_value = token

        request_data = {
            'email': 'john@gmail.com',
            'password': '123456',
        }

        response = self.client.post(
            '/auth/login',
            data=simplejson.dumps(request_data),
            content_type='application/json',
        )

        response_dict = simplejson.loads(response.get_data(as_text=True))

        assert_that(response.status_code).is_equal_to(200)
        assert_that(response_dict['access_token']).is_equal_to(token.decode())

        authenticate.assert_called_with(
            request_data['email'],
            request_data['password'],
        )

    @mock.patch('main.service.auth_service.authenticate')
    def test_login_invalid_credentials(self, authenticate):
        """Test login fails with invalid credentials."""
        authenticate.return_value = None

        request_data = {
            'email': 'john@gmail.com',
            'password': '123456',
        }

        response = self.client.post(
            '/auth/login',
            data=simplejson.dumps(request_data),
            content_type='application/json',
        )

        response_dict = simplejson.loads(response.get_data(as_text=True))

        assert_that(response.status_code).is_equal_to(200)
        assert_that(response_dict).contains_key('message')
        assert_that(response_dict).does_not_contain_key('access_token')
