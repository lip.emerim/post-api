"""Define user blueprint tests."""

from unittest import mock

import simplejson
from assertpy import assert_that

from main.model.user_model import UserModel
from test.base_test import BaseTest


class UserBlueprintTest(BaseTest):
    """Define user blueprint tests."""

    @mock.patch('main.service.user_service.save')
    def test_save_user(self, save):
        """Test the save user method flow."""
        user = UserModel(email='romero@gmail.com', password='123avcsaw#')

        save.return_value = user

        request_data = {
            'email': user.email,
            'password': user.password,
        }

        response = self.client.post(
            '/user',
            data=simplejson.dumps(request_data),
            content_type='application/json',
        )

        response_dict = simplejson.loads(response.get_data(as_text=True))

        assert_that(response.status_code).is_equal_to(201)
        assert_that(response_dict['email']).is_equal_to(user.email)
        assert_that(response_dict).does_not_contain_key('password')
        assert_that(save.called).is_true()

    @mock.patch('main.service.user_service.save')
    def test_save_user_invalid_data(self, save):
        """Test the save user method flow with invalid data."""
        user = UserModel(email='nope', password='not really')

        request_data = {
            'email': user.email,
            'password': user.password,
        }

        response = self.client.post(
            '/user',
            data=simplejson.dumps(request_data),
            content_type='application/json',
        )

        assert_that(response.status_code).is_equal_to(422)
        assert_that(save.called).is_false()

    @mock.patch('main.service.user_service.save')
    def test_save_user_existing_email(self, save):
        """Test the save_user method flow with an existing email."""
        user = UserModel(email='john@gmail.com', password='asdas12312@')

        request_data = {
            'email': user.email,
            'password': user.password,
        }

        response = self.client.post(
            '/user',
            data=simplejson.dumps(request_data),
            content_type='application/json',
        )

        assert_that(response.status_code).is_equal_to(422)
        assert_that(save.called).is_false()
