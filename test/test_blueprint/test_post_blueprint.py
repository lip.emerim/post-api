"""Define post blueprint tests."""

from unittest import mock
from unittest.mock import ANY

import simplejson
from assertpy import assert_that

from main.model.post_model import PostModel
from main.model.user_model import UserModel
from test.base_test import BaseTest


class PostBlueprintTest(BaseTest):
    """Define post blueprint tests."""

    @mock.patch('main.service.post_service.find_by_id')
    def test_find_by_id(self, find_by_id):
        """Test the find_by_id method flow."""
        post = PostModel.query.first()

        find_by_id.return_value = post

        response = self.client.get(
            f'/post/{post.id}',
            content_type='application/json',
        )

        response_dict = simplejson.loads(response.get_data(as_text=True))

        assert_that(response.status_code).is_equal_to(200)
        assert_that(response_dict['id']).is_equal_to(post.id)
        assert_that(response_dict['title']).is_equal_to(post.title)
        find_by_id.assert_called_with(post.id)

    @mock.patch('main.service.post_service.search')
    def test_search(self, search):
        """Test the search method flow with valid data."""
        post = PostModel.query.first()
        search.return_value = {'items': [post]}

        tag = 'sadas'
        start = 0
        length = 5

        response = self.client.get(
            f'/post?tag={tag}&start={start}&length={length}',
            content_type='application/json',
        )

        response_dict = simplejson.loads(response.get_data(as_text=True))

        assert_that(response.status_code).is_equal_to(200)
        assert_that(response_dict['items']).is_length(1)

        search.assert_called_with(start, length, tag)

    @mock.patch('main.service.post_service.search')
    def test_search_invalid_params(self, search):
        """Test the search method flow with invalid data."""
        tag = ''
        start = -1
        length = -34

        response = self.client.get(
            f'/post?tag={tag}&start={start}&length={length}',
            content_type='application/json',
        )
        assert_that(response.status_code).is_equal_to(422)
        assert_that(search.called).is_false()

    @mock.patch('main.service.auth_service.verify_auth_token')
    @mock.patch('main.service.post_service.save')
    def test_save_post(self, save, auth):
        """Test the save_post method flow with valid data."""
        user = UserModel(email='romero@gmail.com', password='12345sds@', id=5)
        post = PostModel(title='very cool post', body='very cool post body and I mean it.')

        save.return_value = post
        auth.return_value = user

        request_data = {
            'title': post.title,
            'body': post.body,
        }

        response = self.client.post(
            '/post',
            data=simplejson.dumps(request_data),
            content_type='application/json',
        )

        response_dict = simplejson.loads(response.get_data(as_text=True))

        assert_that(response.status_code).is_equal_to(201)
        assert_that(response_dict['title']).is_equal_to(post.title)
        assert_that(response_dict['body']).is_equal_to(post.body)
        save.assert_called_with(ANY, user.id)
        assert_that(auth.called).is_true()

    @mock.patch('main.service.auth_service.verify_auth_token')
    @mock.patch('main.service.post_service.save')
    def test_save_post_invalid_params(self, save, auth):
        """Test the save_post method flow with invalid data."""
        user = UserModel(email='romero@gmail.com', password='12345sds@', id=5)
        auth.return_value = user

        request_data = {
            'title': 'sa',
            'body': 'bu',
        }

        response = self.client.post(
            '/post',
            data=simplejson.dumps(request_data),
            content_type='application/json',
        )
        assert_that(response.status_code).is_equal_to(422)
        assert_that(save.called).is_false()

    @mock.patch('main.service.auth_service.verify_auth_token')
    @mock.patch('main.service.post_service.save')
    def test_save_post_not_logged_in(self, save, auth):
        """Test the save_post method flow with an unauthorized user."""
        request_data = {
            'title': 'dsfsddsfs',
            'body': 'fsfdsfdsfdsfsd',
        }

        auth.return_value = None

        response = self.client.post(
            '/post',
            data=simplejson.dumps(request_data),
            content_type='application/json',
        )
        assert_that(response.status_code).is_equal_to(401)
        assert_that(save.called).is_false()

    @mock.patch('main.service.auth_service.verify_auth_token')
    @mock.patch('main.service.post_service.delete')
    def test_delete_post(self, delete, auth):
        """Test the delete_post method flow."""
        user = UserModel(email='romero@gmail.com', password='12345sds@', id=5)
        auth.return_value = user
        post_id = 1

        response = self.client.delete(
            f'/post/{post_id}',
            content_type='application/json',
        )

        assert_that(response.status_code).is_equal_to(204)
        delete.assert_called_with(post_id, user.id)
        assert_that(auth.called).is_true()

    @mock.patch('main.service.auth_service.verify_auth_token')
    @mock.patch('main.service.post_service.delete')
    def test_delete_post_not_logged_in(self, save, auth):
        """Test the delete method flow with an unauthorized user."""
        auth.return_value = None

        response = self.client.delete(
            '/post/1',
            content_type='application/json',
        )
        assert_that(response.status_code).is_equal_to(401)
        assert_that(save.called).is_false()
