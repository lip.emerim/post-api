"""WSGI instance of flask application, used to run it via uWSGI."""
from main.factory import create_app

app = create_app()

if __name__ == '__main__':
    app.run()
